import { TestingApiExperimentsPage } from './app.po';

describe('testing-api-experiments App', () => {
  let page: TestingApiExperimentsPage;

  beforeEach(() => {
    page = new TestingApiExperimentsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
