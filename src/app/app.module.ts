import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AsyncDataComponent } from './async-data/async-data.component';
import {testableHttpProvider} from "./testable-http.service";

@NgModule({
  declarations: [
    AppComponent,
    AsyncDataComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [testableHttpProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
