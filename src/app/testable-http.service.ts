import {Injectable, Testability} from '@angular/core';
import {ConnectionBackend, Http, RequestOptions, RequestOptionsArgs, XHRBackend} from "@angular/http";

@Injectable()
export class TestableHttpService extends Http {

  constructor(_backend: ConnectionBackend, _defaultOptions: RequestOptions,
              private testability: Testability) {
    super(_backend, _defaultOptions)
  }

  get(url: string, options?: RequestOptionsArgs) {

    this.testability.increasePendingRequestCount();

    return super.get(url, options)
      .do(() => this.testability.decreasePendingRequestCount());
  };

}

export const testableHttpProvider = {
  provide: Http, useClass: TestableHttpService,
  deps: [XHRBackend, RequestOptions, Testability]
};
