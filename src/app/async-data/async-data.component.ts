import {Component, OnInit} from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/pluck';

@Component({
  selector: 'app-async-data',
  templateUrl: './async-data.component.html',
  styleUrls: ['./async-data.component.css']
})
export class AsyncDataComponent implements OnInit {

  data$: Observable<any>;
  data: any;

  constructor(http: Http) {

    this.data$ = http.get('https://x-announcer.firebaseio.com/x-announcer/reports/-KQ5kaxpz6zShYePsoA8.json')
      .delay(10)
      .map(r => r.json())
      .do(console.log);

    this.data$.subscribe(data => this.data = data)
  }

  ngOnInit() {
  }

}
