import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .then(() => {

    const w = window as any;
    const [appRoot] = w.getAllAngularRootElements();

    const testability = w.getAngularTestability(appRoot);
    console.log(testability.getPendingRequestCount());

    testability.whenStable(() => {
      console.log('application isStable');
      window['prerenderReady'] = true;
    })

  });
